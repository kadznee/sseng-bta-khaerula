/**
 * Database connection infrastructure
 */
(function(db){

    var mongoose = require('mongoose');
    var config = require('../../myconfig.json');

    var init = function (){
        var cstring = config['defaultConnection'];
        console.log('Mongo db connected on: ' + config['defaultConnection']);
        mongoose.connect(cstring);        
    };

    init(); // call initialization on 1st time loading

    db.getSchema = function(schemaName, schemaObj){
        try {
            if(mongoose.model(schemaName)){
                return mongoose.model(schemaName);
            }
        }
        catch(e) {
            if (e.name === 'MissingSchemaError') {
                return mongoose.model(schemaName, schemaObj);
            }
        }
    };
})(module.exports);