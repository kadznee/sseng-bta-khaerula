/**
 * Table schema (data model) for category
 */

(function (categorySchema) {

    var mongoose = require('mongoose');

    categorySchema.schema = mongoose.Schema({
        name: String,
        code: String,
        parentCode: String
    });

})(module.exports);