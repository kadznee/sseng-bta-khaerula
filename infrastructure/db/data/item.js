/**
 * Table schema (data model) for item
 */

(function (itemSchema) {
    
    var mongoose = require('mongoose');

    itemSchema.schema = mongoose.Schema({
        name: String,
        description: String,
        image: String,
        price: Number,
        size: String,
        color: String,
        categoryCode: String,
        productCode: String 
    });
        
})(module.exports);