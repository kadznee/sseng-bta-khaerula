/**
 * Test methods for category service
 */

(function (categoryTests) {

    var assert = require('chai').assert;
    var categoryService = require('../../domain/services/categoryService');

    var testCategory = {
        name: 'Test Category',
        code: 'T001',
        parentCode: '',
        testId: ''
    };

    categoryTests.run = function(){
        describe('Categories Service Domain', function() {
            describe('Initial execution', function() {
                it('should successfully clean up test data: ' + testCategory.code, function() {
                     categoryService.getCategoryByCode(
                        testCategory.code, function(err, result){
                        assert.equal(null, err);
                        assert.notEqual(null, result);
                        for(var i in result){
                            categoryService.removeCategory(result[i]._id, function(err){
                                assert.equal(null, err);
                            })
                        }
                    })
                });
            });
            
            describe('Create new category', function() {
                it('should successfully create a new category: ' + testCategory.code, function() {
                    categoryService.createNewCategory(testCategory, function(err){
                        assert.equal(null, err);
                    })
                });

                it('should successfully check the newly created category: ' + testCategory.code , function() {
                    categoryService.getCategoryByCode(testCategory.code, function(err, result){
                        assert.equal(null, err);
                        assert.notEqual(null, result);
                        assert.notEqual(0, result.length);
                        testCategory.testId = result[0]._id;
                    });
                });

                it('should not create a new category if the code already existed: ' + testCategory.code, function() {
                    categoryService.createNewCategory(testCategory, function(err){
                        assert.notEqual(null, err);
                    })
                });
            });

            describe('Update existing category', function() {                 
                it('should not update the category if update input is null', function() {
                    categoryService.updateCategory(testCategory.testId, undefined, function(err){
                        assert.notEqual(null, err);
                        categoryService.getCategoryById(testCategory.testId, function(err, result){
                            assert.equal(null, err);
                            assert.notEqual(null, result);
                            assert.notEqual(0, result.length);
                            assert.equal(testCategory, result[0]);
                        });
                    })
                });

                it('should not update the category if it does not existed', function() {
                    categoryService.updateCategory('testCategory.testId', undefined, function(err){
                        assert.notEqual(null, err);
                        categoryService.getCategoryById(testCategory.testId, function(err, result){
                            assert.equal(null, err);
                            assert.notEqual(null, result);
                            assert.notEqual(0, result.length);
                            assert.equal(testCategory, result[0]);
                        });
                    })
                });

                it('should successfully update the new category: ' + testCategory.code, function() {
                    categoryService.updateCategory(testCategory.testId, {
                        name: 'Updated Test Category',
                        code: 'T001',
                        parentCode: ''               
                    }, function(err){
                        assert.equal(null, err);
                    })
                });        
            });

            describe('Remove the test category: '+ testCategory.code, function() {
                it('should not remove non-existing category', function() {
                    categoryService.removeCategory('testCategory.testId', function(err){
                        assert.equal(null, err);
                    })
                });
                it('should successfully remove the category: ' + testCategory.code, function() {
                    categoryService.removeCategory(testCategory.testId, function(err){
                        assert.equal(null, err);
                    })
                });
            });
        });
    };

})(module.exports)