/**
 * Test methods for products service
 */

(function (productTests) {

    var assert = require('chai').assert;
    var productService = require('../../domain/services/productService');

    var testProduct = {
        name: 'Test Product',
        description: 'Test Product code PT001 under categoryCode T001',
        categoryCode: 'T001',
        productCode: 'PT001',
        size: 'XXL',
        color: 'Red',
        price: 185000,
        testId: ''
    };

    productTests.run = function(){
        describe('Products Service Domain', function() {
            describe('Initial execution', function() {
                it('should successfully clean up test products under test category: ' + testProduct.productCode, function() {
                     productService.getProductByCategoryCode(
                        testProduct.categoryCode, function(err, result){
                        assert.equal(null, err);
                        assert.notEqual(null, result);
                        for(var i in result){
                            productService.removeProduct(result[i]._id, function(err){
                                assert.equal(null, err);
                            })
                        }
                    })
                });
            });

            describe('Create new product', function() {
                it('should successfully create a new product: ' + testProduct.productCode , function() {
                    productService.createNewProduct(testProduct, function(err){
                        assert.equal(null, err);
                    });                    
                });

                it('should successfully check the newly created product: ' + testProduct.productCode , function() {
                    productService.getProductByFilter({ productCode: testProduct.productCode }, function(err, result){
                        assert.equal(null, err);
                        assert.notEqual(null, result);
                        assert.notEqual(0, result.length);
                        testProduct.testId = result[0]._id;
                    });        
                });

                it('should not create a new product if the code already existed: ' + testProduct.productCode , function() {
                    productService.createNewProduct(testProduct, function(err){
                        assert.notEqual(null, err);
                    });                    
                });                
            });

            describe('Get product by filter', function() {
                it('should successfully get a new product with color: ' + testProduct.color , function() {
                    productService.getProductByFilter({
                        color: testProduct.color                        
                    }, function(err, result){
                        assert.equal(null, err);
                        assert.notEqual(null, result);
                        assert.equal(result.color, testProduct.color);
                    })
                });

                it('should successfully get a new product with size: ' + testProduct.size , function() {
                    productService.getProductByFilter({
                        size: testProduct.size                        
                    }, function(err, result){
                        assert.equal(null, err);
                        assert.notEqual(null, result);
                        assert.equal(result.color, testProduct.color);
                    })
                });

                it('should successfully get a new product with price range: 0 - 200,000' , function() {
                    productService.getProductByFilter({
                        price: { $gt: 0, $lt: 200000 }                        
                    }, function(err, result){
                        assert.equal(null, err);
                        assert.notEqual(null, result);
                        assert.equal(result.color, testProduct.color);
                    })
                });
            });

            describe('Update existing product', function() {
                
                it('should not update the product if update input is null', function() {
                    productService.updateProduct(testProduct.testId, undefined, function(err){
                        assert.notEqual(null, err);
                        productService.getProductByFilter({
                            productCode: testProduct.productCode
                        }, function(err, result){
                            assert.equal(null, err);
                            assert.notEqual(null, result);
                            assert.notEqual(0, result.length);
                             // result object is still the same, no update
                            assert.equal(testProduct, result[0]);
                        });
                    })
                });

                it('should not update the product if it does not existed', function() {
                    productService.updateProduct(testProduct.testId, undefined, function(err){
                        assert.notEqual(null, err);
                        productService.getProductByFilter({
                            productCode: testProduct.productCode
                        }, function(err, result){
                            assert.equal(null, err);
                            assert.notEqual(null, result);
                            assert.notEqual(0, result.length);
                            assert.equal(testProduct, result[0]);
                        });
                    })
                });
                
                it('should successfully update the new product: ' + testProduct.productCode, function() {
                    productService.updateProduct(testProduct.testId, {
                        name: 'Test Product Updated',
                        description: 'Update Test Product code PT001 under categoryCode T001',
                        categoryCode: 'T001',
                        productCode: 'PT001',
                        size: 'XXL',
                        color: 'Blue',
                        price: 190000                    
                    }, function(err){
                        assert.equal(null, err);
                    })
                });

                
            });

            describe('Remove the test product: '+ testProduct.productCode, function() {
                it('should not remove non-existing product', function() {
                    productService.removeProduct(testProduct.testId, function(err){
                        assert.equal(null, err);
                    })
                });
                
                it('should successfully remove the product: ' + testProduct.productCode, function() {
                    productService.removeProduct(testProduct.testId, function(err){
                        assert.equal(null, err);
                    })
                });
            });
        });
    };

})(module.exports)