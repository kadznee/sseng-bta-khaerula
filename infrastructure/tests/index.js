/**
 * The unit test for the services
 */
var categoryTests = require('./categoryTests.js');
var productTests = require('./productTests.js');

categoryTests.run();
productTests.run();