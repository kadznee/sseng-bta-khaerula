var port = process.env.PORT || 5000;

var express = require('express');
var app = express();
var bodyParser = require('body-parser');

/* body parser */
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/* Different router for different API version (API versioning) */
var routerv1 = express.Router();
var routerv2 = express.Router();

var controllers = require("./controllers");
controllers.init(routerv1, 'v1');
controllers.init(routerv2, 'v2');
// etc...

app.use('/v1', routerv1);
app.use('/v2', routerv2);
app.use('/', routerv2);

/* Start the server */
app.listen(port, function () {
  console.log('API running on port ' + port);
});