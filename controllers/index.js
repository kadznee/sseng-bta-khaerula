/**
 * Api controller version switch
 * Please use API version 2
 */

(function(controller){
    controller.init = function(router, apiversion){
        if(apiversion === 'v1'){
            var apiv1 = require('./v1');
            apiv1.init(router);
        }
        if(apiversion === 'v2'){
            var apiv2 = require('./v2');
            apiv2.init(router);
        }
    };
})(module.exports);