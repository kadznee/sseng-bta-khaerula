/**
 * Api version 1
 * Controller/routing collection
 */

(function(apiv1){
    var categoryController = require('./categoryController');
    var productController = require('./productController');

    apiv1.init = function (router) {
        categoryController.init(router);
        productController.init(router);
    };

})(module.exports);