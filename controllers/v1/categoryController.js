/**
 * Category controller Version 1 
 * This controller has NO token authentication
 * Consumer of the application service
 */

(function(categoryController){

    var express = require('express');
    var categoryService = require('../../domain/services/categoryService');

    categoryController.init = function(router){
        router.use('/categories', express.Router()            
            .get('/', function (req, res) {
               categoryService.getCategoriesByParentCode(null, function(err, result){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json(result);
               })
            })
            .get('/:id', function (req, res) {
               categoryService.getCategoryById(req.params.id, function(err, result){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json(result);
               })
            })
            .post('/', function (req, res) {
                categoryService.createNewCategory(req.body, function(err){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json({message: "Successfully Saved!"});
               })
            })
            .put('/:id', function (req, res) {    
                categoryService.updateCategory(req.params.id, req.body, function(err){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json({message: "Successfully Updated!"});
               })
            })
            .delete('/:id', function (req, res) {    
                categoryService.removeCategory(req.params.id, function(err){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json({message: "Successfully Deleted!"});
               })
            })
	    );
    };
})(module.exports);