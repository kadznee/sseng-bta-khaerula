/**
 * Product controller Version 1
 * This controller has NO token authenticate
 * Consumer of the application service
 */

(function(productController){

    var express = require('express');
    var productService = require('../../domain/services/productService'); 

    productController.init = function(router){
        router.use('/products', express.Router()
            .get('/', function (req, res) {            
               productService.getProductByCategoryCode(null, function(err, result){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json(result);
               })
            })
            .get('/:id', function (req, res) {
               productService.getProductByProductId(req.params.id, function(err, result){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json(result);
               })
            })
            .post('/', function (req, res) {
                productService.createNewProduct(req.body, function(err){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json({message: "Successfully Saved!"});
               })
            })
            .put('/:id', function (req, res) {    
                productService.updateProduct(req.params.id, req.body, function(err){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json({message: "Successfully Updated!"});
               })
            })
            .delete('/:id', function (req, res) {    
                productService.removeProduct(req.params.id, function(err){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json({message: "Successfully Deleted!"});
               })
            })
	    );
    };
})(module.exports);