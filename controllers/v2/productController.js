/**
 * Product controller Version 2
 * This controller has token authentication
 * Consumer of the application service
 */

(function(productController){

    var express = require('express');
    var config = require('../../myconfig.json');
    var productService = require('../../domain/services/productService'); 

    productController.init = function(router){
        router.use('/products', express.Router()
            .use(function(req, res, next){
                // middleware to authenticate this API endpoint
                if(req.query['token'] != config['securityToken']){
                    next('Error: Failed to load products resource! invalid security token! (please add ?token=60b3a3ea9a66ff85ab6a3a804a9a151a to each end of your requests )');
                }
                next();
            })
            .get('/', function (req, res) {            
               productService.getProductByCategoryCode(null, function(err, result){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json(result);
               })
            })
            .get('/:id', function (req, res) {
               productService.getProductByProductId(req.params.id, function(err, result){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json(result);
               })
            })
            .post('/', function (req, res) {
                productService.createNewProduct(req.body, function(err){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json({message: "Successfully Saved!"});
               })
            })
            .put('/:id', function (req, res) {    
                productService.updateProduct(req.params.id, req.body, function(err){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json({message: "Successfully Updated!"});
               })
            })
            .delete('/:id', function (req, res) {    
                productService.removeProduct(req.params.id, function(err){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json({message: "Successfully Deleted!"});
               })
            })
	    );
    };
})(module.exports);