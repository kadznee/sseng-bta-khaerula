/**
 * Category controller Version 2 
 * This controller has token authentication
 * Consumer of the application service
 */

(function(categoryController){

    var express = require('express');
    var config = require('../../myconfig.json');
    var categoryService = require('../../domain/services/categoryService');

    categoryController.init = function(router){
        router.use('/categories', express.Router()
            .use(function(req, res, next){
                // middleware to authenticate this API endpoint
                if(req.query['token'] != config['securityToken']){
                    next('Error: Failed to load categories resource! invalid security token! (please add ?token=60b3a3ea9a66ff85ab6a3a804a9a151a to each end of your requests )');
                }
                next();
            })
            .get('/', function (req, res) {
               categoryService.getCategoriesByParentCode(null, function(err, result){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json(result);
               })
            })
            .get('/:id', function (req, res) {
               categoryService.getCategoryById(req.params.id, function(err, result){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json(result);
               })
            })
            .post('/', function (req, res) {
                categoryService.createNewCategory(req.body, function(err){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json({message: "Successfully Saved!"});
               })
            })
            .put('/:id', function (req, res) {    
                categoryService.updateCategory(req.params.id, req.body, function(err){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json({message: "Successfully Updated!"});
               })
            })
            .delete('/:id', function (req, res) {    
                categoryService.removeCategory(req.params.id, function(err){
                   if(err){
                       res.json(err);
                       return;
                   }
                   res.json({message: "Successfully Deleted!"});
               })
            })
	    );
    };
})(module.exports);