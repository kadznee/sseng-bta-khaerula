/**
 * Api version 2
 * Controller/routing collection
 * Different API version may have different controller collection
 */

(function(apiv2){  
    var categoryController = require('./categoryController');
    var productController = require('./productController');

    apiv2.init = function (router) {
        categoryController.init(router);
        productController.init(router);
    };

})(module.exports);