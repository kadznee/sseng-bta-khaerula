General Info:
---
- sseng-bta-khaerula
    - sseng: SaleStock Engineering
    - bta: Backend Technical Assessment
    - khaerula: testee name: Khaerul Adzany
- khaerul.adzany@gmail.com - backend engineer technical assessment - 15th July 2016
- case study #1
- main language used in this test: javascript (using node.js)

Description:
---
- This software adapts Onion Architecture for DDD approach using NodeJs, ExpressJs and MongoDb
    - domain/core : core layer, contains the business entities
    - domain/models : domain layer, contains the business model
    - domain/repository : domain layer, contains the generic repository
    - domain/services : application service (API) layer, the entry point to domain layer
    - infrastructure : infrastructure layer, containing database adapters, tests
    - controllers : contains all the controllers used by external consumer, part of the infrastructure layer
- This software also have API versioning implemented, for example:
    - To access API version 1 for category, the url is: `localhost/v1/categories`
    - To access API version 2 for category, the url is: `localhost/v2/categories`
- This software also have security key token authentication (for API version 2 only, use API version 1 to bypass token authentication)
    - Security key token to access the API is `60b3a3ea9a66ff85ab6a3a804a9a151a`
    - Please always use `?token=60b3a3ea9a66ff85ab6a3a804a9a151a` query parameter for all requests

Quick Start:
---
- To run unit test, execute: `npm test`
- To run the API, execute: `npm start` (please make sure you have running mongodb instance)
    - To change mongodb configuration, edit the `infrastructure/db/dbconfig.json` file
- available endpoints are:
    - Categories
        - (GET, POST) http://host/v2/categories (you can try to change v2 to v1 to test API versioning)
        - (GET, PUT, DELETE) http://host/v2/categories/id
    - Products
        - (GET, POST) http://host/v2/products
        - (GET, PUT, DELETE) http://host/v2/products/id
- To test the API, we can use programs such as:
    - postman (chrome extension)
    - dhc explorer (chrome extension)
    - Telerik fiddler
- To run API functional test, please open the script `api_functional_testing.saz` using Telerik fiddler
