/**
 * Domain business model for the product/item management
 * Business process/logic resides here
 */

(function(productDomainModel){    

    var repo = require('../repository');
    var product = require('../../infrastructure/db/data/item');    
    var productEntity = require('../core/entities/product');
    
    var setting = {};

    function init(){
        setting = {
            name: productEntity.entity.name,
            schema: product.schema
        }
    };

    init();

    productDomainModel.createNewProduct = function(newProduct, callback){
        repo.findByFilter({ productCode: newProduct.productCode }, setting, function(err, result){
            
            if(err){
                callback({ error: err });
                return;
            }
            
            if(result){
                callback({ error: 'cannot insert duplicate product with code: ' + newProduct.productCode });
                return;
            }
            
            repo.add(newProduct, setting, callback);
        });
        
    };

    productDomainModel.removeProduct = function(prid, callback){
        repo.findOne(prid, setting, function(err, result){
	        if(err){
                callback({ error: err });
                return;
            }
            
            if(!result){
                callback({ error: 'there is no product with id: ' + prid });
                return;
            }
            
            repo.removeById(prid, setting, callback);
        });        
    };

    productDomainModel.updateProduct = function(prid, curProduct, callback){
        
        if(!curProduct) { 
            callback({ error: 'please specify current product!' }); 
            return;
        } 

        if(curProduct && curProduct._id){
            callback({ error: 'updating id is prohibited!' });
            return;
        }
        
        repo.findOne(prid, setting, function(err, result){
            if(err){
                callback({ error: err });
                return;
            }
            
            if(!result){
                callback({ error: 'there is no product with id: ' + prid });
                return;
            }

            repo.update(prid, curProduct, setting, callback);
        });

        
    };

})(module.exports)