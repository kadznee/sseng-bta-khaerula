/**
 * Domain business model for the category management
 * Business process/logic resides here
 */

(function(categoryDomainModel){
    
    var repo = require('../repository');
    var category = require('../../infrastructure/db/data/category');    
    var categoryEntity = require('../core/entities/category');
    
    var setting = {};

    function init(){
        setting = {
            name: categoryEntity.entity.name,
            schema: category.schema
        };
    }

    init();

    categoryDomainModel.createNewCategory = function(newCategory, callback){
        repo.findByFilter({code: newCategory.code}, setting, function(err, result){
            if(err){
                callback({ error: err });
                return;
            }
            
            if(result){
                callback({ error: 'cannot insert duplicate categpry with code: ' + newCategory.code });
                return;
            }

            repo.add(newCategory, setting, callback);
        });
        
    };

    categoryDomainModel.removeCategory = function(cid, callback){
        repo.findOne(cid, setting, function(err, result){
	        if(err){
                callback({ error: err });
                return;
            }
            
            if(!result){
                callback({ error: 'there is no category with id: ' + cid });
                return;
            }
            
            repo.removeById(cid, setting, callback);
        });        
    };

    categoryDomainModel.updateCategory = function(cid, curCategory, callback){
        
                
        if(!curCategory) {
            callback({ error: 'please specify current category!' });
            return;
        }

        if(curCategory && curCategory._id){
            callback({ error: 'updating id is prohibited!' });
            return;
        } 


        repo.findOne(cid, setting, function(err, result){
	        if(err){
                callback({ error: err });
                return;
            }
            
            if(!result){
                callback({ error: 'there is no category with id: ' + cid });
                return;
            }
            
            repo.update(cid, curCategory, setting, callback);
        });        
    };

})(module.exports)