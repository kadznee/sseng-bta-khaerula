/**
 * Generic repository for the domains
 * Database infrastructure: mongoosejs
 */

(function(repo){

    var db = require('../../infrastructure/db');

    repo.add = function(input, setting, callback){
        var schema = db.getSchema(setting.name, setting.schema);
        var newObj = new schema(input);
        newObj.save(callback);
    }

    repo.removeById = function(oid, setting, callback){
        var schema = db.getSchema(setting.name, setting.schema);
        schema.remove({ _id: oid }, callback);
    }
    
    repo.findOne = function(oid, setting, callback){
        var schema = db.getSchema(setting.name, setting.schema);
        schema.findOne({ _id: oid}, callback);
    }

     repo.findByFilter = function(filter, setting, callback){
        var schema = db.getSchema(setting.name, setting.schema);
        schema.findOne(filter, callback);
    }

    repo.update = function(oid, obj, setting, callback){
        var schema = db.getSchema(setting.name, setting.schema);
        schema.update({ _id: oid}, obj, callback);
    }

})(module.exports);