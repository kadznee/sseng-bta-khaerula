/**
 * Product Service
 * Gateway for controller to access domain model (business process)
 * Query accessed db and data model directly
 * Commands abstracted inside domain model
 */

(function (productService) {

    var db = require('../../infrastructure/db');
    var item = require('../../infrastructure/db/data/item');
    var productDM = require('../models/productDomainModel');
    var productEntity = require('../core/entities/product');

    productService.getProductByCategoryCode = function(ccode, callback){
        var schema = db.getSchema(productEntity.entity.name, item.schema);

        var filter = {};
        if(ccode) filter = { categoryCode: ccode };

        schema.find(filter, callback);
    };

    productService.getProductByProductId = function(prid, callback){
        var schema = db.getSchema(productEntity.entity.name, item.schema);

        if(!prid) callback({ error: 'product id not specified!' });

        schema.find({ _id: prid }, callback);
    };

    productService.getProductByFilter = function(filter, callback){
        var schema = db.getSchema(productEntity.entity.name, item.schema);

        if(!filter) callback({ error: 'something wrong with your filter!' });

        schema.find(filter, callback);
    };

    productService.createNewProduct = function(newProduct, callback){
        productDM.createNewProduct(newProduct, function(err){
            if(err) {
                callback(err);
                return;
            }
            callback(null);
        });
    };

    productService.removeProduct = function(prid, callback){
        productDM.removeProduct(prid, function(err){
            if(err) {
                callback(err);
                return;
            }
            callback(null);
        });
    };

    productService.updateProduct = function(prid, curProduct, callback){
        productDM.updateProduct(prid, curProduct, function(err){
            if(err) {
                callback(err);
                return;
            }
            callback(null);
        });
    };
    
})(module.exports);