/**
 * Category Service
 * Gateway for controller to access domain model (business process)
 * Query accessed db and data model directly
 * Commands abstracted inside domain model
 */

(function (categoryService) {
    
    var db = require('../../infrastructure/db');
    var category = require('../../infrastructure/db/data/category');
    var categoryDM = require('../models/categoryDomainModel');
    var categoryEntity = require('../core/entities/category');

    categoryService.getCategoriesByParentCode = function(pcode, callback){
        var schema = db.getSchema(categoryEntity.entity.name, category.schema);

        if(!pcode) pcode = '';

        schema.find({ parentCode: pcode }, callback);
    };

    categoryService.getCategoryById = function(cid, callback){
        var schema = db.getSchema(categoryEntity.entity.name, category.schema);
        schema.find({ _id: cid }, callback);
    };

    categoryService.getCategoryByCode  = function(ccode, callback){
        var schema = db.getSchema(categoryEntity.entity.name, category.schema);
        schema.find({ code: ccode }, callback);
    };

    categoryService.createNewCategory = function(newCategory, callback){
        categoryDM.createNewCategory(newCategory, function(err){
            if(err) {
                callback(err);
                return;
            }
            callback(null);
        });
    };

    categoryService.removeCategory = function(cid, callback){
        categoryDM.removeCategory(cid, function(err){
            if(err) {
                callback(err);
                return;
            }
            callback(null);
        });
    };

    categoryService.updateCategory = function(cid, curCategory, callback){
        categoryDM.updateCategory(cid, curCategory, function(err){
            if(err) {
                callback(err);
                return;
            }
            callback(null);
        });
    };

})(module.exports);