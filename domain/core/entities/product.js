/**
 * Domain model for products
 * This will provide constant value for mongodb schema name
 */

(function(product){
    
    product.entity = {
        name: 'Product'
    };

})(module.exports);