/**
 * Domain model for categories
 * This will provide constant value for mongodb schema name
 */

(function(category){

    category.entity = {
        name: 'Category'
    };

})(module.exports);